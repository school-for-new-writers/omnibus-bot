"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var discord_js_1 = require("discord.js");
var axios_1 = __importDefault(require("axios"));
function sendDerpi(chan, query, color) {
    axios_1.default.get("https://derpibooru.org/search.json?q=" + query + "%2C+safe+score.gt:10&random_image=y").then(function (r) {
        axios_1.default.get("https://derpibooru.org/" + r.data.id + ".json").then(function (r) {
            var embed = new discord_js_1.RichEmbed()
                .setTitle('🔗 Source')
                .setURL("https://derpibooru.org/" + r.data.id)
                .setColor(color)
                .setImage('https:' + r.data.image);
            chan.send(embed)
                .catch(console.error);
        })
            .catch(console.error);
    })
        .catch(console.error);
}
exports.sendDerpi = sendDerpi;
//# sourceMappingURL=derpi.js.map