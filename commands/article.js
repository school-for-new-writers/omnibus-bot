"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var discord_js_1 = require("discord.js");
var axios_1 = __importDefault(require("axios"));
var Fuse = require("fuse.js");
var fs = require('fs');
function findArticle(query, chan) {
    fs.readFile('./articlestore.json', function (err, data) {
        if (err) {
        }
        else {
            var articles = JSON.parse(data.toString('utf8'));
            var options = {
                shouldSort: true,
                // includeScore: true,
                threshold: 0.6,
                location: 0,
                distance: 100,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                keys: [
                    "title"
                ]
            };
            var fuse = new Fuse(articles, options);
            var result = fuse.search(query);
            var embed_1 = new discord_js_1.RichEmbed()
                .setTitle("Here's your results!")
                .setFooter("Find all at https://sfnw.site");
            result
                .splice(-5)
                .reverse()
                .forEach(function (e) {
                embed_1.addField(e.title, "By " + e.author + " \u2013 [\uD83D\uDD17 Read](https://sfnw.site/" + e.url + ")");
            });
            chan.send(embed_1)
                .catch(console.error);
        }
    });
}
exports.findArticle = findArticle;
function updateStore(user) {
    axios_1.default.get('https://sfnw.site/api.json').then(function (r) {
        fs.writeFile('./articlestore.json', JSON.stringify(r.data), function (err) {
            if (err) {
                user.createDM().then(function (r) {
                    r.send("**An error has occurred:**\n```json\n" + err + "\n```");
                });
            }
            else {
                user.createDM().then(function (r) {
                    r.send("Store updated successfully!");
                });
            }
        });
    });
}
exports.updateStore = updateStore;
//# sourceMappingURL=article.js.map